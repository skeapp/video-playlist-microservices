package playlist

import (
	"encoding/json"
	"../getData"
)

type ChildAttribute struct{
	Value string `json:"value"`
	Parent string `json:"parent"`
}

type Child struct{
	ID string `json:"id"`
	Type string `json:"type"`
	ChildAtt ChildAttribute `json:"attributes"`	
}
	
type Attribute struct{
	Name string `json:"name"`
	Children []Child `json:"children"`
}

type Playlist struct{
	ID string `json:"id"`
	Type string `json:"type"`
	Attributes Attribute `json:"attributes"`
}

type PlaylistList struct {
	Playlist Playlist `json:"data"`
}

/*
	Creates the Playlist struct and returns as []byte
*/
func CreatePlayJSON(pID string)[]byte{
	
	var childrenArray = []Child{}	
	videoList := getData.GetPlaylistsVideos(pID)
	
	for _,commentEl := range videoList {
		xc := ChildAttribute{Value:commentEl["name"],Parent: pID}	
		pcx := Child{ID:commentEl["id"],ChildAtt:xc,Type: "video"}
		childrenArray = append(childrenArray,pcx)						
	}

	playListInfo := getData.GetPlaylistInfo(pID)

	attribute := Attribute{Name: playListInfo["name"],Children: childrenArray}
	playlistob := Playlist{ID: playListInfo["id"],Type: "playlist",Attributes: attribute}	


	playlistJSON := PlaylistList{
		Playlist: playlistob,
	}
	js, _ := json.Marshal(playlistJSON)
	return js
}
