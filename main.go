/*
	Author: Iordanis Paschalidis
	EndPoint for System 2
*/
package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"./output"
	"fmt"
)


/*
	returns json data for specific video - need video's id in the request 
	request example: http://127.0.0.1:8085/video/4

	response example:
	{
		data: {
			id: "103",
			type: "video",
			attributes: {
				name: "Iordanis Video",
				url: "youtube.com/AD324ASD"
				children: [
					{
						id: "9103",
						type: "comment",
						attributes: {
							value: "This is a comment",
							parent: "103"
						}
					},
					{
						id: "9104",
						type: "comment",
						attributes: {
							value: "This is a comment too",
							parent: "103"
						}
					},
			}
		}
	}
	The children array contains all comments (in a json object ) of the video

*/
func getVideo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idA := vars["id"]
	fmt.Println(idA)
	//calling GetJSONOutput which returns []byte of a json object
	w.Header().Set("Content-Type", "application/vnd.api+json")	
	w.Write(output.GetJSONOutput("Video",idA))
}


/*
	returns json data for specific playlist - need playlist's id in the request 
	request example: http://127.0.0.1:8085/playlist/4

	response example:
	{
		data: {
			id: "4",
			type: "playlist",
			attributes: {
				name: "Iordanis Playlist",
				children: [
					{
						id: "103",
						type: "video",
						attributes: {
							name: "Iordanis Video",
							parent: "4"
						}
					}
			}
		}
	}
	The children array contains all video (in a json object ) of the playlist

*/
func getPlaylist(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idA := vars["id"]
	//calling GetJSONOutput which returns []byte of a json object
	w.Header().Set("Content-Type", "application/vnd.api+json")	
	w.Write(output.GetJSONOutput("Playlist",idA))
}


func getAllPlaylist(w http.ResponseWriter, r *http.Request) {
	//calling GetJSONOutput which returns []byte of a json object
	w.Header().Set("Content-Type", "application/vnd.api+json")	
	w.Write(output.GetJSONOutput("PlaylistAll",""))
}

/*
	running on the localhost port 8085
*/
func main() {
	server := http.Server{
		Addr: "127.0.0.1:8085",
	}
	
	r := mux.NewRouter()

	//routes
	r.HandleFunc("/playlistAll", getAllPlaylist)	
	r.HandleFunc("/playlist/{id}", getPlaylist)	
	r.HandleFunc("/video/{id}", getVideo)	


	http.Handle("/", r)
	
	server.ListenAndServe()
}