package allPlaylist

import (
	"encoding/json"
	"../getData"
)
	
type Attribute struct{
	Name string `json:"name"`
}

type Playlist struct{
	ID string `json:"id"`
	Type string `json:"type"`
	Attributes Attribute `json:"attributes"`
}

type PlaylistList struct {
	Playlist []Playlist `json:"data"`
}

/*
	Creates the PlaylistList struct and returns as []byte
*/
func CreatePlayJSON()[]byte{
	var playlistobArray = []Playlist{}

	playLists := getData.GetPlaylists()

	for _, playList := range playLists {
		attribute := Attribute{Name: playList["name"]}
		playlistob := Playlist{ID: playList["id"],Type: "playlists",Attributes: attribute}	
		playlistobArray = append(playlistobArray,playlistob)
	}
	
	playListJSON := PlaylistList{
		Playlist: playlistobArray,
	}
	js, _ := json.Marshal(playListJSON)
	return js
}
