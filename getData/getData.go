package getData 

import (
	"encoding/json"
    "os"
	"fmt"
)	

type Plc struct {
	Playlists []struct {
		Name  string `json:"name"`
		ID    string `json:"id"`
		Video []struct {
			VID     string `json:"vid"`
			Title   string `json:"title"`
			URL     string `json:"url"`
			Comment []struct {
				Value string `json:"value"`
				Cid   string `json:"cid"`
			} `json:"comment"`
		} `json:"video"`
	} `json:"playlists"`
}



/*
	load data from data file and return them in a Plc struct usage : 
*/
func loadData(file string) Plc {
    var data Plc
	dataFile, err := os.Open(file)
    defer dataFile.Close()
    if err != nil {
        fmt.Println(err.Error())
    }
	jsonParser := json.NewDecoder(dataFile)

    jsonParser.Decode(&data)
    return data
}

func GetVideo(id string) map[string]string{
    video := make(map[string]string)
	var playlists = loadData("pcv.json")
	for _,playlistElement := range playlists.Playlists {
		for _,videoElement := range playlistElement.Video {
			if(id == videoElement.VID){
				video["VID"] = videoElement.VID
				video["title"] = videoElement.Title
				video["url"] = videoElement.URL
			}
		}
	}
	return  video
}

func GetComments(vid string) [] map[string]string{
	var comments [] map[string]string

	var playlists = loadData("pcv.json")
	for _,playlistElement := range playlists.Playlists {
		for _,videoElement := range playlistElement.Video {
			if(vid == videoElement.VID){
				for _,commentEl := range videoElement.Comment {
					comment := make(map[string]string)
					comment["Cid"] = commentEl.Cid
					comment["Value"] = commentEl.Value
					comments = append(comments, comment)

				}
			}
		}
	}
	return comments
}

func GetPlaylistInfo(id string) map[string]string{
    playlist := make(map[string]string)
	var playlists = loadData("pcv.json")
	for _,playlistElement := range playlists.Playlists {
		playlist["name"] = playlistElement.Name
		playlist["id"] = playlistElement.ID	
	}
	return playlist
}

func GetPlaylistsVideos(pid string) [] map[string]string{
	var videos [] map[string]string

	var playlists = loadData("pcv.json")
	for _,playlistElement := range playlists.Playlists {
		if(pid == playlistElement.ID){
			for _,videoElement := range playlistElement.Video {	
				video := make(map[string]string)
				video["id"] = videoElement.VID
				video["name"] = videoElement.Title
				videos = append(videos, video)
			}
		}
	}
	return videos
}

func GetPlaylists() [] map[string]string{
	var playlistsArr [] map[string]string

	var playlists = loadData("pcv.json")
	for _,playlistElement := range playlists.Playlists {
		playlist := make(map[string]string)
		playlist["id"] = playlistElement.ID
		playlist["name"] = playlistElement.Name
		playlistsArr = append (playlistsArr,playlist)
	}
	return playlistsArr
}