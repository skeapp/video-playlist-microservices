# playlistAPI

Endpoint for the Palylist API 


## Prerequisites

Need to install 	"github.com/gorilla/mux" library

go get "github.com/gorilla/mux"


### End points 

-------
All playlist

```
http://localhost:8085/playlistAll
```

Data for specific playlist
```
http://localhost:8085/playlist/id
example:
http://localhost:8085/playlist/pl1
```



Data for specific video

```
http://localhost:8085/video/id
example:
http://localhost:8085/video/v2
```