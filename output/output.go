/*
	Author: Iordanis Paschalidis
*/
package output

//package output

import (
	"../video"
	"../playlist"
	"../allPlaylist"
)

/*
	Used to return json objects for the end points 

	/playlist/id	-- pass parameters GetJSONOutput("Playlist",playlistID)
	/video/id		-- pass parameters GetJSONOutput("Video",videoID)
	/video/id		-- pass parameters GetJSONOutput("Video",videoID)

	returns []byte of the json object

*/
func GetJSONOutput(typeOfGetter string,guid string)[]byte{
	var js []byte
	switch {
		case typeOfGetter == "Playlist":
			js = playlist.CreatePlayJSON(guid)
		case typeOfGetter == "Video":
			js = video.CreateVideoJSON(guid)
		case typeOfGetter == "PlaylistAll":
			js = allPlaylist.CreatePlayJSON()
		}
	return js	
}
