package video

import (
	"encoding/json"
	"../getData"
)

type ChildAttribute struct{
	Value string `json:"value"`
	Parent string `json:"parent"`
}

type Child struct{
	ID string `json:"id"`
	Type string `json:"type"`
	ChildAtt ChildAttribute `json:"attributes"`	
}
	
type Attribute struct{
	Name string `json:"name"`
	URL string `json:"url"`
	Children []Child `json:"children"`
}

type Video struct{
	ID string `json:"id"`
	Type string `json:"type"`
	Attributes Attribute `json:"attributes"`
}

type VideoList struct {
	VideoData Video `json:"data"`
}

/*
	Creates the VideoList struct and returns as []byte
*/
func CreateVideoJSON(vidID string)[]byte{
	
	var childrenArray = []Child{}//will contrain all comments of the array
	commentList := getData.GetComments(vidID)//graps the comments from the model
	
	//goes through the comments of the video and Create Child struct which are appeneding in the childrenArray
	for _,commentEl := range commentList {
		xc := ChildAttribute{Value:commentEl["Value"],Parent: vidID}	
		pcx := Child{ID:commentEl["Cid"],ChildAtt:xc,Type: "comment"}
		childrenArray = append(childrenArray,pcx)						
	}

	//gets video info 
	videoInfo := getData.GetVideo(vidID)

	//add video info to the Video and Attribute struct
	attribute := Attribute{Name: videoInfo["title"],URL: videoInfo["url"],Children: childrenArray}
	video := Video{ID: videoInfo["VID"],Type: "video",Attributes: attribute}	

	//final  struct for the json object
	videoJSON := VideoList{
		VideoData: video,
	}
	js, _ := json.Marshal(videoJSON)
	return js
}
